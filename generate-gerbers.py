#!/usr/bin/python3

import pcbnew
import os

if not os.path.exists("./gerber_output"):
  os.mkdir("./gerber_output")

os.chdir("./kicad_design")

board_name = "openipmc-hw-breakout-board"
board_pcb  = pcbnew.LoadBoard("dimm_breakout.kicad_pcb")
pctl = pcbnew.PLOT_CONTROLLER(board_pcb)
enabled_layers = board_pcb.GetEnabledLayers()

layer_ids = list(enabled_layers.Seq())

layer_names = [ board_pcb.GetLayerName(layer_id) for layer_id in layer_ids ]

#max_string_len = max(layer_names, key=len)

os.chdir("../")

os.chdir("./gerber_output")


for i, layer_id in enumerate(layer_ids):
  pctl.SetLayer(layer_id)
  layer_name      = board_pcb.GetLayerName(layer_id).replace(".", "_")
  gerber_filename = str(layer_id).zfill(2) + "-" + layer_name
  pctl.OpenPlotfile(gerber_filename, pcbnew.PLOT_FORMAT_GERBER, layer_name)
  pctl.PlotLayer()

